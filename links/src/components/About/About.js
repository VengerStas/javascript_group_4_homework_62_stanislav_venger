import React from 'react';
import './About.css';
import Navigation from "../Navigation/Navigation";

const About = () => {
    return (
        <div id="about-us">
            <Navigation/>
            <div className="info-block">
                <div className="about-us">
                    <p>About Us<span>We are added our major Services</span></p>
                </div>
                <div className="items">
                    <div className="container">
                        <div className="item">
                            <h4>
                                <span className="facebook">Facebook Marketing</span>
                            </h4>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsum esse, nulla dolores ex
                                laudantium recusandae.</p>
                        </div>
                        <div className="item">
                            <h4>
                                <span className="landing">Landing pages Creation</span>
                            </h4>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repudiandae, autem voluptate
                                quas iusto at eligendi?</p>
                        </div>
                        <div className="item">

                            <h4>
                                <span className="ads">Ads management</span>
                            </h4>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Pariatur id eius qui neque
                                quidem alias!</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default About;