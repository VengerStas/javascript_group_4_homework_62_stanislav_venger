import React from 'react';
import './Home.css';
import Navigation from "../Navigation/Navigation";
import PhotoOne from '../../assets/img/photo-1.jpg';
import PhotoTwo from '../../assets/img/photo-2.jpg';

const Home = () => {
    return (
        <div id="home">
            <Navigation/>
            <div className="main-pic">
                <div className="main-pic-info">
                    <div className="container">
                        <h4>Lastest News</h4>
                        <span>Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus…</span>
                    </div>
                </div>
            </div>
            <div className="container">
                <div className="main-info-1 clearfix">
                    <h4>our services<span>Blazrobar.com</span></h4>
                    <img src={PhotoOne} alt="main-pic" />
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. A quo quaerat unde est ut nemo
                            inventore voluptate quis, dolorum reprehenderit dignissimos amet eveniet, modi. Dicta
                            obcaecati sapiente quasi non ea dolores optio, nobis totam, recusandae, debitis inventore
                            provident sequi eius. Laborum voluptatibus nobis reiciendis. Temporibus nihil eaque
                            veniam?</p>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quos est quod voluptate earum sed
                            debitis consequuntur iure nisi quaerat deserunt ut accusamus magni hic eaque voluptatibus
                            quisquam, magnam aliquam doloribus qui ullam sit vitae minima. Delectus ducimus ratione
                            eaque rerum eveniet quibusdam consequatur, nostrum autem nesciunt hic qui repellat neque
                            asperiores, quia ab.</p>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laboriosam sapiente illo similique
                            distinctio molestiae quaerat. Maiores odit ullam possimus, accusamus, ea aspernatur cum
                            velit. Accusantium numquam earum doloribus, blanditiis itaque, deserunt consequuntur
                            explicabo minima, voluptas voluptatibus necessitatibus possimus hic laboriosam dolores
                            libero.</p>
                </div>
                <div className="main-info-2 clearfix">
                    <h4>Unique Design<span>Shoe house</span></h4>
                    <img className="photo" src={PhotoTwo} alt="photo2" />
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repellendus a minus, reiciendis
                            quidem facere, doloribus ea quo provident debitis? Dignissimos atque error, iusto fugiat
                            perferendis eius corrupti, eligendi suscipit sapiente minima pariatur, asperiores! Numquam
                            ea tempore accusantium cumque temporibus debitis placeat ut esse, mollitia quaerat quidem
                            nemo, corporis autem omnis!</p>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sunt dolorem voluptas optio, rem
                            soluta inventore ea tempora veniam fugit, vel sit quibusdam explicabo ut consequatur cum
                            quisquam totam odio nihil ipsam! Accusamus sed cum incidunt delectus, aliquam quo quas,
                            minus autem quidem officiis, ab provident corporis veniam consequuntur, ipsa soluta?</p>
                </div>
                <div className="main-info-3 clearfix">
                    <img className="photo" src={PhotoTwo} alt="photo2" />
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quaerat velit voluptas provident
                            perferendis possimus perspiciatis fugiat, sequi incidunt vel, aut ipsum. Adipisci reiciendis
                            sunt tempora amet repudiandae eius temporibus ipsa harum blanditiis quo illum numquam
                            cumque, sint, alias similique aperiam porro ut, consequatur! Adipisci eum nisi ipsa,
                            obcaecati asperiores optio.</p>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsum ex, velit accusamus eveniet
                            in illo quidem praesentium omnis voluptatibus impedit vero vel nobis corporis rem quaerat
                            iusto possimus deleniti facere ipsam! Dignissimos praesentium aliquid facilis, aut, soluta
                            provident placeat id beatae, quas, quae itaque atque tenetur asperiores optio corporis
                            odio?</p>
                </div>
            </div>
        </div>
    );
};

export default Home;