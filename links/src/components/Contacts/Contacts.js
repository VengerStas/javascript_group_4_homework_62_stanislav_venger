import React from 'react';
import './Contacts.css';
import Navigation from "../Navigation/Navigation";

const Contacts = () => {
    return (
        <div className="contacts">
           <Navigation/>
            <footer className="footer">
                <div className="container">
                    <div className="blocks">
                        <div className="block">
                            <h4 className="footer-title">Recent Posts</h4>
                            <p className="footer-recent__text"><span className="footer-recent__text--link">Wedding
                                Photos that Will Blow Your Mind Away.</span>
                            </p>
                            <p className="footer-recent__text"><span className="footer-recent__text--link">Intent
                                WordPress Theme Features are Amazing.</span>
                            </p>
                            <p className="footer-recent__text"><span className="footer-recent__text--link">A New
                                Era of Design is Here</span>
                            </p>
                            <p className="footer-recent__text"><span className="footer-recent__text--link">Creativio
                                WordPress Theme now Available for Sale.</span>
                            </p>
                        </div>
                        <div className="block">
                            <h4 className="footer-title">About INTENT</h4>
                            <p className="intent__text">Lorem ipsum dolor sit amet, at mea eius corpora, at eam munere
                                facilisi omittantur. Nemore accusam usu no, ei nec vidit commune constituto. Te usu
                                inani volumus, eum albucius menandri at, sed apeirian percipitur ad.</p>
                            <p className="intent__text">Id vitae regione has. In tollit dicant vim. His ex detracto
                                percipitur, id vis nibh quas illum. Delicata dissentiet ius ne, latine euripidis
                                quaerendum et vis.</p>
                        </div>
                        <div className="block">
                            <h4 className="footer-title">Social Media</h4>
                            <div className="social-media">
                                <p className="icon-rss"></p>
                                <p className="icon-facebook"></p>
                                <p className="icon-twitter"></p>
                                <p className="icon-email"></p>
                                <p className="icon-tumblr"></p>
                                <p className="icon-pth"></p>
                                <p className="icon-vimeo"></p>
                                <p className="icon-zerply"></p>
                                <p className="icon-picasa"></p>
                                <p className="icon-stumble"></p>
                                <p className="icon-skype"></p>
                                <p className="icon-reddit"></p>
                                <p className="icon-evernote"></p>
                                <p className="icon-behance"></p>
                                <p className="icon-dribble"></p>
                                <p className="icon-digg"></p>
                                <p className="icon-blogger"></p>
                                <p className="icon-flickr"></p>
                                <p className="icon-linkedin"></p>
                                <p className="icon-google"></p>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
        </div>
    );
};

export default Contacts;