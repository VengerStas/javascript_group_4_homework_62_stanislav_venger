import React from 'react';
import LogoImg from "../../assets/img/logo.png";
import {NavLink} from "react-router-dom";
import './Navigation.css';

const Navigation = () => {
    return (
        <nav className="nav clearfix">
            <img src={LogoImg} className="logo" alt="logo"/>
            <ul className="nav-menu">
                <li className="nav-item">
                    <NavLink className="nav-link" to="/">Home</NavLink>
                </li>
                <li className="nav-item">
                    <NavLink className="nav-link" to="/about-us">About us</NavLink>
                </li>
                <li className="nav-item">
                    <NavLink className="nav-link" to="/contacts">Contacts</NavLink>
                </li>
            </ul>
        </nav>
    );
};

export default Navigation;