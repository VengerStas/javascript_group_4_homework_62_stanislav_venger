import React, { Component } from 'react';
import {BrowserRouter, Switch, Route} from 'react-router-dom';
import './App.css';
import Home from "../components/Home/Home";
import Contacts from "../components/Contacts/Contacts";
import About from "../components/About/About";

class App extends Component {
  render() {
    return (
        <BrowserRouter>
            <Switch>
                <Route path="/" exact component={Home}/>
                <Route path="/about-us" exact component={About}/>
                <Route path="/contacts" exact component={Contacts}/>
            </Switch>
        </BrowserRouter>
    );
  }
}

export default App;
